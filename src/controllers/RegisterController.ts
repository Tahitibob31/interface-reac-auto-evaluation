import { Request, Response } from "express-serve-static-core";

import { db } from "../server";

export default class RegisterController
{
    static showFormReg(req: Request, res: Response): void
    {
        
        res.render('pages/register', {
            title: 'Register',
            

        });
    }
    static enregister(req: Request, res: Response): void
    {
        let nameOfUsers = db.prepare('SELECT ("username") FROM user').all()
        nameOfUsers = nameOfUsers.map(e => String(e.username));
        let userName = req.body.username;
        let passWord = req.body.password;
        console.log(userName, passWord);
        if(!nameOfUsers.includes(userName)){
            db.prepare('INSERT INTO user ("username", "password") VALUES (?, ?)').run(userName, passWord);
        }
        else{
            
            RegisterController.showFormRegEchec(req, res);
        }
    }
    static showFormRegEchec(req: Request, res: Response): void
    {
        res.render('pages/register', {
            title: "Register",
            messageCreation: "Ce nom est déjà utilisé !!!!!!!"

        });
    }
}
