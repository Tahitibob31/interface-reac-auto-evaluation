import { Request, Response } from "express-serve-static-core";

import { db } from "../server";

export default class CompetenceController
{
    static competence(req: Request, res: Response): void
    {
        const smdb = db.prepare('SELECT * FROM competences').all();
        
        res.render('pages/competence', {
            title: 'Competence',
            competence : smdb
            

        });
    }
}
