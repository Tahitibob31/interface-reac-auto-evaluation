BEGIN;
CREATE TABLE "user"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "username" VARCHAR(45) NOT NULL,
  "password" VARCHAR(45) NOT NULL
);
CREATE TABLE "competences"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(45) NOT NULL,
  "content" VARCHAR(255) NOT NULL
);
CREATE TABLE "criteres"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(45) NOT NULL,
  "competences_id" INTEGER NOT NULL,
  CONSTRAINT "fk_criteres_competences1"
    FOREIGN KEY("competences_id")
    REFERENCES "competences"("id")
);
CREATE INDEX "criteres.fk_criteres_competences1_idx" ON "criteres" ("competences_id");
CREATE TABLE "user_has_criteres"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "user_id" INTEGER NOT NULL,
  "criteres_id" INTEGER NOT NULL,
  CONSTRAINT "fk_user_has_criteres1_user1"
    FOREIGN KEY("user_id")
    REFERENCES "user"("id"),
  CONSTRAINT "fk_user_has_criteres1_criteres1"
    FOREIGN KEY("criteres_id")
    REFERENCES "criteres"("id")
);
CREATE INDEX "user_has_criteres.fk_user_has_criteres1_criteres1_idx" ON "user_has_criteres" ("criteres_id");
CREATE INDEX "user_has_criteres.fk_user_has_criteres1_user1_idx" ON "user_has_criteres" ("user_id");
COMMIT;